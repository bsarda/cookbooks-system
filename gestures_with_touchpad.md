# GESTURES WITH TOUCHPAD

Install the synaptics package

```shell
sudo apt install xserver-xorg-input-synaptics
```

add yourself to the input group
```shell
sudo gpasswd -a $(whoami) input
```

Log in / log out to confirm (or reboot)

go in the configuration: /usr/share/X11/xorg.conf.d/
in the file:

```shell
sudo vi 51-synaptics-quirks.conf
/usr/share/X11/xorg.conf.d/51-synaptics-quirks.conf
```

add:

```shell
Section "InputClass"
    Identifier "SynPS/2 Synaptics TouchPad"
    MatchProduct "SynPS/2 Synaptics TouchPad"
    MatchIsTouchpad "on"
    MatchOS "Linux"
    MatchDevicePath "/dev/input/event*"
    Option "Ignore" "on"
EndSection`
```

in the file:

```shell
sudo vi 70-synaptics.conf
sudo vi /usr/share/X11/xorg.conf.d/70-synaptics.conf
```

comment the lines to have:

```shell
##Section "InputClass"
##        Identifier "touchpad catchall"
##        Driver "synaptics"
##        MatchIsTouchpad "on"
# This option is recommend on all Linux systems using evdev, but cannot be
# enabled by default. See the following link for details:
# http://who-t.blogspot.com/2010/11/how-to-ignore-configuration-errors.html
#       MatchDevicePath "/dev/input/event*"
##EndSection`
```

install libinput-tools

```shell
sudo apt-get install libinput-tools
git clone http://github.com/bulletmark/libinput-gestures
cd libinput-gestures
sudo ./libinput-gestures-setup install
```

start and add the autostart   

```shell
libinput-gestures-setup start
libinput-gestures-setup autostart
```

remove

```shell
cd ..
rm -Rf libinput-gestures/
```

edit the gestures to remove natural move (which inverse l/r and u/d)

```shell
sudo nano /etc/libinput-gestures.conf
```

instead of gesture swipe left, put gesture swipe right, etc..


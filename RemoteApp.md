# REMOTE APP FOR SEAMLESS INTEGRATION

## activate remote app

on the windows10 desktop, activate remote app  
edit the registry key  

```text
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Terminal Server\TSAppAllowList
```

set to `1` the key `fDisabledAllowList`

## network improvement on windows 10

better handling the connection on windows10:

```shell
netsh int tcp set global autotuninglevel=highlyrestricted
```

## installing latest freerdp

installing the latest version from git  
dependancies (is that really required ?)

```shell
sudo apt-get install -y build-essential cmake xsltproc libssl-dev libx11-dev libxext-dev libxinerama-dev
sudo apt-get install -y libxcursor-dev libxdamage-dev libxv-dev libxkbfile-dev libasound2-dev libcups2-dev libxml2 libxml2-dev
sudo apt-get install -y libxrandr-dev libxi-dev libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev
sudo apt-get install -y libcunit1-dev libdirectfb-dev xmlto libxtst-dev
```

installing:

```shell
cd /tmp
git clone git://github.com/FreeRDP/FreeRDP.git
cd FreeRDP
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON .
make
sudo make install
/usr/local/bin/xfreerdp
```

## useful options of freerdp
+sec-ext
+nego
+fonts
-themes
-wallpaper
+clipboard
/app:"||word"

/workarea
+window-drag
/w:1920 /h:1080


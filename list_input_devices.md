# LIST THE INPUT DEVICES

```shell
egrep -i 'DLL|synap|alps|etps|elan' /proc/bus/input/devices
sudo apt-get install libinput-tools xserver-xorg-input-libinput
sudo /usr/bin/libinput-list-devices
sudo xinput | grep -i touchpad
cat /proc/bus/input/devices
```


# Remove thumbs.db

```shell
sudo find / -name 'Thumbs.db' -print0 2>/dev/null | xargs -0 rm --
```

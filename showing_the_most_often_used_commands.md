# SHOWING THE MOST OFTEN USED COMMANDS

```shell
cut -f1 -d" " .bash_history | sort | uniq -c | sort -nr | head -n 30
```


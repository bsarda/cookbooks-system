# Disable SELINUX

## Temporary

go to root

```shell
su
```

set to permissive

```shell
setenforce 0
```

## Definitively

edit the configuration file

```shell
vi /etc/sysconfig/selinux
```

set the line to `SELINUX=disabled`  

you must reboot to take it in account


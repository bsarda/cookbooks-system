# Set static IP for RHEL/Centos 7.x system

edit the file in `/etc/sysconfig/network-scripts/ifcfg-{{something}}`, where something is the name of your ethernet card (could be eth0, ens33, ...)

change the `BOOTPROTO=dhcp` to `BOOTPROTO=none`

update with the following sample (adapt to yours)

```shell
IPADDR=192.168.1.195
PREFIX=24
GATEWAY=192.168.1.1
DNS1=192.168.1.9
DNS2=8.8.8.8
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
```

restart network
```shell
systemctl restart network
```

# WORD ERASE

to enable ctrl+backspace for erasing word by word:

```shell
echo '# word erase with ctrl+backspace' | sudo tee --append /etc/profile > /dev/null
echo 'stty werase ^H' | sudo tee --append /etc/profile > /dev/null
```


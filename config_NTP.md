in the file

```console
/etc/ntp.conf
```

add ` version 3 iburst` after each line contains "server[...]"  
add `tos maxdist 30` at the end of the file

verifiy associations:

```shell
watch ntpq -np -c assoc
```

# How to make Microsoft Teams works on Linux

_thanks to : https://techcommunity.microsoft.com/t5/Microsoft-Teams/TEAMS-and-LINUX-The-Future-is-Now/td-p/251876_

## Browser

Donwload and install Chrome or Chromium.  

## Extensions

Install the extention **"User Agent Switcher for Chrome"**

## Add the user agent strings

In the freshly installed extension, on the icon in the toolbar > righclic > **Options**  
Enter the new user-agent name, for exemple **"Teams special"**  
Enter the new user-agent string, exactly : **"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134"**  
Enter the group, for exemple **"Chrome"**  
Select **Replace**  
Set the indicator flag (it's the 3 letters that you will see on the icon), for exemple **"TEA"**  

## Enable the Agent-Switcher

Click the User Agent Switcher and choose the created agent string.  
This should remain selected until you change it to something else or back to the browser default.  

## Set the flags

Open the Chrome browser, in the address bar type **chrome://flags** and hit the Enter key.  
In the search box provided, search for each of the settings below:

- Hardware Acceleration 
  - Override software rendering list: ENABLED 
- PWA 
  - Enable PWA full code cache: ENABLED 
  - Desktop PWAs: ENABLED 
  - Desktop PWAs Link Capturing: ENABLED 
- WEBRTC 
  - Negotiation with GCM cipher suites for SRTP in WebRTC: ENABLED 
  - Negotiation with encrypted header extensions for SRTP in WebRTC: ENABLED 
  - WebRTC Stun origin header: ENABLED 
  - WebRTC Echo Canceller 3: ENABLED 
  - WebRTC new encode cpu load estimator: ENABLED 
  - WebRTC H.264 software video encoder/decoder: ENABLED 
- Downloads 
  - Parallel downloading: ENABLED 

## Verify

Open Microsoft Teams in your browser.  
Start a private chat with someone and verify that the video chat icon switches from grey to purple and white. If so, you can start making video calls and you should also be able to make presentation.  


# USEFUL ALIASES

edit the file

```shell
nano ~/.bash_aliases
```

add:

```shell
#!/bin/bash
# Adding custom aliases
# Adding custom aliases
alias l='ls -lrt'
alias ll='ls -l'
alias lwc='ls -lrt | wc -l'
alias la='ls -lart'
alias lm='ls -lrt | more'
alias lwc='ls | wc -l'
alias f='find . -name $1'
alias ..='cd ..'
alias k='kubectl '
alias kg='kubectl get -o wide' 
alias dl='docker ps -a'
alias drc="docker ps -a | grep -v Up | grep -v \"CONTAINER ID\" | awk '{ system(\"docker rm \" \$1)}'"
alias dri="docker images | grep -v \"IMAGE ID\" | awk '{ system(\"docker rmi \"\$3)}'"
alias dnp='docker network prune'
alias gad='git add .;git commit -m "bsarda sync";git push'
alias updc='sudo apt update && sudo apt list --upgradable'
alias updg='sudo apt update && sudo apt upgrade -y'
alias clearhist='rm ~/.local/share/recently-used.xbel'
alias kmem='free -h && sudo sysctl -w vm.drop_caches=3 && sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches && free -h'
alias peyte="printf '\e[31m        \e[105m@@@@@@\e[49m           \e[105m@@@@@@\e[49m\n      \e[105m@@@@@@@@@@\e[49m       \e[105m@@@@@@@@@@\e[49m\n    \e[105m@@@@@@@@@@@@@@\e[49m   \e[105m@@@@@@@@@@@@@@\e[49m\n  \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n\e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n\e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n\e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n  \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n   \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n    \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n      \e[105m@@@@@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n        \e[105m@@@@@@@@@@@@@@@@@@@@@@@\e[49m\n          \e[105m@@@@@@@@@@@@@@@@@@@\e[49m\n            \e[105m@@@@@@@@@@@@@@@\e[49m\n              \e[105m@@@@@@@@@@@\e[49m\n                \e[105m@@@@@@@\e[49m\n                  \e[105m@@@\e[49m\n                   \e[105m@\e[49m\n\n'"
```


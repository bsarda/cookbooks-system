# Ubuntu 18.04 DNS

## Issue

with -at least this version- there could be a bug where the DNS server and resolver are badly implemented.

It uses 127.0.0.1:53 , local resolver/forwarder instead of the corp's DNS server.

## The default

```console
ubuntu@ubuntu:~$ ls -l /etc/resolv.conf
lrwxrwxrwx 1 root root 39 Apr 26 12:07 /etc/resolv.conf -> ../run/systemd/resolve/stub-resolv.conf
```

## Solution

Deleted this and re-point to the correct file, then reboot.

```console
ubuntu@ubuntu:~$ sudo rm -f /etc/resolv.conf
ubuntu@ubuntu:~$ sudo ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
ubuntu@ubuntu:~$ ls -l /etc/resolv.conf
lrwxrwxrwx 1 root root 32 May 29 08:48 /etc/resolv.conf -> /run/systemd/resolve/resolv.conf
```

Then reboot

```shell
sudo reboot
```

# convert PST to MBOX files

MBOX are largely read-able by several software, including Thunderbird.
PST is the type of mail archive from MS Outlook.

Install the pst-utils

```shell
sudo apt install pst-utils
```

Go to the folder where you have all your PST files, then execute this one-liner

```shell
for i in *.pst ; do fnam="$(echo $i | tr '[:upper:]' '[:lower:]' | sed -e 's/\.[^.]*$//')" ; echo "processing: $fnam" ; mkdir "$fnam" ; readpst -o "$fnam/" -q "$i" ; done
```

Then open your mbox files :)
sample (https://www.howtogeek.com/709718/how-to-open-an-mbox-file-in-mozilla-thunderbird/)[https://www.howtogeek.com/709718/how-to-open-an-mbox-file-in-mozilla-thunderbird/]

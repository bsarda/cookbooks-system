# CEDILLE ON THE C

Editing the files

```shell
sudo nano /usr/lib/x86_64-linux-gnu/gtk-3.0/3.0.0/immodules.cache
```

or

```shell
sudo nano /usr/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/immodules.cache
```

change the line to add 'en' 

```shell
sudo sed -i "s/az:ca:co:fr:gv:oc:pt:sq:tr:wa/az:ca:co:fr:gv:oc:pt:sq:tr:wa:en/" /usr/lib/x86_64-linux-gnu/gtk-3.0/3.0.0/immodules.cache
```

replace "ć" to "ç" and "Ć" to "Ç" in /usr/share/X11/locale/en_US.UTF-8/Compose

```shell
sudo cp /usr/share/X11/locale/en_US.UTF-8/Compose /usr/share/X11/locale/en_US.UTF-8/Compose.bak
sed 's/ć/ç/g' < /usr/share/X11/locale/en_US.UTF-8/Compose | sed 's/Ć/Ç/g' > Compose
sudo mv Compose /usr/share/X11/locale/en_US.UTF-8/Compose
```

add the lines in the file /etc/environment

```shell
echo 'GTK_IM_MODULE=cedilla' | sudo tee --append /etc/environment > /dev/null
echo 'QT_IM_MODULE=cedilla' | sudo tee --append /etc/environment > /dev/null
```

reboot


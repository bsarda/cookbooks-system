# Install linux tinycore

Goal is to install linux tinycore in a vm, for a minimal distro.

1. download the binary from [https://distro.ibiblio.org/tinycorelinux/downloads.html](https://distro.ibiblio.org/tinycorelinux/downloads.html) , select "core"

2. create the vm: 256MB of memory (at least for install), >=100MB of disk, os=other 4.x linux 32bit

3. boot from cdrom

4. load the installer

```shell
tce-load -wi tc-install
```

5. sudo tc-install.sh

6. select:

- Frugal **(f)**
- whole disk **(1)**
- install bootloader **(y)**
- dont install extensions tce/cde **(n)**
- select ext4 **(3)**
- add the options:**(vga=788 tce=hda1)**
- start install **(y)**

7. install openssh:

```shell
tce-load -iw openssh
cd /usr/local/etc/ssh
sudo cp ssh_config.example ssh_config
sudo cp sshd_config.example sshd_config
sudo /usr/local/etc/init.d/openssh start
```

8. change passords for users to be able to connect:  

- change tc pass:

```shell
passwd`
```

- change root pass:

```shell
sudo passwd`
```

9. install nano and vmtools

```shell
tce-load -iw nano
tce-load -iw open-vm-tools
```

10. add ip config at boot

```shell
echo '/opt/eth0.sh &' | sudo tee -a /opt/bootlocal.sh > /dev/null
```

11. set static ip

```shell
cat << EOF >> /opt/eth0.sh
#!/bin/sh
pkill udhcpc
ifconfig eth0 192.168.63.111
route add default gw 192.168.63.2
echo nameserver 192.168.63.4 > /etc/resolv.conf
EOF
sudo chown root:staff /opt/eth0.sh
sudo chmod +x /opt/eth0.sh
```

12. ensure the correct settings are init

```shell
cat /etc/sysconfig/tcedir/onboot.lst
cat /opt/.filetool.lst
```

13. do a backup
```
filetool.sh -b
```
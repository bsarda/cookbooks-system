# Unable to copy the user's Xauthorization file

in case of this message:

```
Unable to copy the user's Xauthorization file"
```

which could happen in the case of VMware workstation when downloading/installing tools (seen in Debian 10 / kernel 4.19 / VMware Workstation 15)

Create the file:

```shell
touch ~/.Xauthority
```

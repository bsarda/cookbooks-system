# Install webmin on a Ubuntu Server

go in root/sudo mode

```shell
sudo -i
```

then execute those commands

```shell
echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
wget -q -O- http://www.webmin.com/jcameron-key.asc | apt-key add
apt update
apt install -y webmin
```


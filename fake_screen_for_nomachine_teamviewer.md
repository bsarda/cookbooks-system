# Add a "fake" screen

Add a "fake" screen to have remote desktop without a physical screen connected to it.

works on Ubuntu 20.04.02 LTS

Install the fake driver

```shell
sudo apt install -y xserver-xorg-video-dummy
```

add this to `/etc/X11/xorg.conf`

```
Section "Device"
    Identifier  "Configured Video Device"
    Driver      "dummy"
EndSection

Section "Monitor"
    Identifier  "Configured Monitor"
    HorizSync 31.5-48.5
    VertRefresh 50-70
EndSection

Section "Screen"
    Identifier  "Default Screen"
    Monitor     "Configured Monitor"
    Device      "Configured Video Device"
    DefaultDepth 24
    SubSection "Display"
    Depth 24
    Modes "1024x800"
    EndSubSection
EndSection
```

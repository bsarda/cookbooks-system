# BASIC TOOLING

```shell
sudo nano /etc/apt/sources.list
```

add the sources *contrib* and *non-free* if not exists

```shell
apt-get update
```

install the basic tools:

```shell
sudo apt install dconf-editor apt-utils gnome-tweaks
sudo apt install unzip tar wget curl git dnsutils ufw gufw
# networking tools
sudo apt install iputils-ping telnet mtr wireshark net-tools
# video tools
sudo apt install vlc mkvtoolnix mkvtoolnix-gui handbrake
# VPN cisco-like client, pptp and openvpn
sudo apt install network-manager-pptp network-manager-pptp-gnome
sudo apt install vpnc network-manager-vpnc-gnome network-manager-vpnc
sudo apt install openvpn network-manager-openvpn-gnome openvpn-systemd-resolved
# docker and kubectl
sudo apt install docker kubectl
# fonts
sudo apt install fonts-linuxlibertine
# printing system
sudo apt install cups
# antivirus
sudo apt install clamav clamtk-gnome clamtk clamav-base clamav-freshclam clamdscan
```

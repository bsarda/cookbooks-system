# POPUP TERM GUAKE

to install a popup terminal, install guake

```shell
sudo apt-get install guake
mkdir -p ~/.config/autostart/
cat << EOF >> ~/.config/autostart/guake.desktop
[Desktop Entry]
Type=Application
Name=guake
Exec=/usr/bin/guake
EOF
```

in case of shortcut (F12 by default) does not work, configure as a system shortcut:
Settings > Devices > Keyboard > add by clicking on +, command `/usr/bin/guake`, shortcut `F12`

# Find all available interfaces

Get all the interfaces names on a linux system

```shell
ip a | awk '/link\/ether/ {print n}{n=$0}' | awk -F: '{print $2}' | sed 's/[[:space:]]//g'
```

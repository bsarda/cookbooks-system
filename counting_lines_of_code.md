# Counting lines of code

```shell
find . -name '*.go' | xargs wc -l
```

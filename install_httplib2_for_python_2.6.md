# Install httplib2 for python 2.6

```shell
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/httplib2/httplib2-0.8.tar.gz
tar -xvf httplib2-0.8.tar.gz -C /tmp
cd /tmp/httplib2-0.8/
python setup.py install
```

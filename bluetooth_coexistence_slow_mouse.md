# BLUETOOTH COLLABORATION / COHEXISTENCE

disable to avoid the mouse lags

```shell
sudo echo "options iwlwifi bt_coex_active=0" > /etc/modprobe.d/iwlwifi.conf
```

restart, check if it's really disabled (must show N):

```shell
cat /sys/module/iwlwifi/parameters/bt_coex_active
```

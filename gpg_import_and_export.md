# Import and export GPG keys

Thanks to:
[Chris Roos](https://gist.github.com/chrisroos/1205934)
[Stackoverflow/JTK](https://stackoverflow.com/questions/53869908/which-private-key-file-in-private-keys-v1-d-directory-belongs-to-which-key)

## Exporting

### Export public and secret key and ownertrust

```sh
gpg -a --export bsarda@mail.com > bsarda-public-gpg.key
gpg -a --export-secret-keys bsarda@mail.com > bsarda-secret-gpg.key
gpg --export-ownertrust > bsarda-ownertrust-gpg.txt
```

## Importing

### Import secret key (which contains the public key) and ownertrust

```sh
gpg --import chrisroos-secret-gpg.key
gpg --import-ownertrust chrisroos-ownertrust-gpg.txt
```

### Ultimately trust the imported key

This is so that I can encrypt data using my public key

```sh
gpg --edit-key bsarda@mail.com
gpg> trust
Your decision? 5 (Ultimate trust)
```

## List the keys

Since GnuPG 2.1 [What's new in 2.1](https://www.gnupg.org/faq/whats-new-in-2.1.html), private keys of GnuPG are stored in the private-keys-v1.d subdirectory.
After experimenting with key creation etc., I found that I have several *.key files in this directory

```sh
ls .gnupg/private-keys-v1.d
xxxxxxxxxxxxxxxxxxxxxxxxxxxx.key
yyyyyyyyyyyyyyyyyyyyyyyyyyyy.key
zzzzzzzzzzzzzzzzzzzzzzzzzzzz.key
```

The file names (x+, y+ and z+) looks like fingerprints, but are not equal to any of my existing public keys.
How can I find which key file in this directory belongs to which key visible with gpg --list-keys?

Answer: Use --with-keygrip option when listing your keys.

```sh
gpg --list-secret-keys --with-keygrip
gpg --list-keys --with-keygrip
```

You can compare than the output with the content of the private-keys-v1.d subdirectory, where the keys are named like <keygrip>.key.
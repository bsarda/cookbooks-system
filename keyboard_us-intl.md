# KEYBOARD US INTL

test the mapping:

```shell
setxkbmap -layout us -variant intl
```

reconfigure default keyboard mapping:

```shell
sudo dpkg-reconfigure keyboard-configuration
```

and the one used by the current user:  
go to `Settings` > `Regions & Languages` > choose `US International with dead keys`  
reset of the input devices

```shell
udevadm trigger --subsystem-match=input --action=change
sudo service keyboard-setup restart
```


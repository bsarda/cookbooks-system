# List vSphere HA events

In powerCLI, after connect-viserver

```powershell
Get-VIEvent -start (Get-Date).addhours(-1) -MaxSamples 10000 | Where-Object {$_.eventtypeid -eq "com.vmware.vc.ha.VmRestartedByHAEvent"}
```

# GITHUB CONFIG

once installed (apt-get...)

```shell
git config --global user.email "b.sarda@free.fr"
git config --global user.name "bsarda"
git config --global color.ui true
git config --global core.editor "atom --wait"
git config --global credential.helper "cache --timeout=999999"
```

add source on folders:

```shell
git init
git remote add origin https://github.com/bsarda/docker-postgresql.git
```


# TOUCHPAD BLOCKING

to disable the touchpad when typing on keyboad:

```shell
sudo apt-get install libinput-tools xserver-xorg-input-libinput
sudo nano /usr/share/X11/xorg.conf.d/40-libinput.conf
```

added from https://gist.github.com/waltervargas/93178b2ac341a1a11d254fe97a2e798c

```shell
Section "InputClass"
        Identifier "DLL06E5:01 06CB:7A13 Touchpad"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Option "Tapping"            "True"
        Option "TappingDrag"        "True"
        Option "DisableWhileTyping" "True"
        Option "TapButton1"            "1"
        Option "TapButton2"            "2"     # multitouch
        Option "TapButton3"            "3"     # multitouch
        ## Option "VertTwoFingerScroll"   "1"     # multitouch
        Option "HorizTwoFingerScroll"  "1"     # multitouch
        Driver "libinput"
EndSection
```


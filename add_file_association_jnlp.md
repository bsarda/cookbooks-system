# Associate .jnlp with javaws

```shell
cat << EOF >> ~/.local/share/applications/javaws-jnlp.desktop
[Desktop Entry]
Type=Application
Name=Java Web Start
MimeType=application/x-java-jnlp-file;
Exec=/usr/bin/javaws %f
Encoding=UTF-8
Terminal=false
StartupNotify=true
Icon=javaws
EOF
```


# Convert PDF to CBZ

```shell
#/bin/bash!
for f in *.pdf; do
	fo=${f::-4}
	mkdir "$fo"
	pdftoppm -jpeg "$f" "$fo"/0
	zip "$fo" "$fo"/*
	mv "$fo".zip "$fo".cbz
done
```


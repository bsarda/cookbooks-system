# Find and delete

## Find older than X days

list the files not modified in the last 30 days

```shell
find . -mtime +30 -print
```

then delete

```shell
find . -mtime +30 -delete
```

## Find extension

```shell
find . -name \*.swp -type f -delete
```


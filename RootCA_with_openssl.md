# creating rootCA with a basic openssl setup

variable to be replaced with your folder

```shell
ROOTCADIR=/home/bsarda/github/rootca
```

## creating the rootCA key and cert

### inspired from

<https://jamielinux.com/docs/openssl-certificate-authority/sign-server-and-client-certificates.html>  
<https://www.sslshopper.com/article-most-common-openssl-commands.html>

### Prepare the directory

```shell
mkdir -p $ROOTCADIR
cd $ROOTCADIR
mkdir certs crl newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial
```

### Preparation of openssl.conf

```shell
cat << EOF > $ROOTCADIR/openssl.conf
[ ca ]
default_ca = CA_default

[ CA_default ]
# Directory and file locations.
dir               = $ROOTCADIR
certs             = $dir/certs
crl_dir           = $dir/crl
new_certs_dir     = $dir/newcerts
database          = $dir/index.txt
serial            = $dir/serial
RANDFILE          = $dir/private/.rand

# The root key and root certificate.
private_key       = $dir/private/ca.key
certificate       = $dir/certs/ca.crt

# For certificate revocation lists.
crlnumber         = $dir/crlnumber
crl               = $dir/crl/ca.crl
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 3650
preserve          = no
policy            = policy_strict

[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of man ca.
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the ca man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the req tool (man req).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
# See https://en.wikipedia.org/wiki/Certificate_signing_request>
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = FR
stateOrProvinceName_default     = IDF
localityName_default            = Paris
0.organizationName_default      = Corp
organizationalUnitName_default = Lab
#emailAddress_default           =

[ v3_ca ]
# Extensions for a typical CA (man x509v3_config).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (man x509v3_config).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:0
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
# Extensions for client certificates (man x509v3_config).
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (man x509v3_config).
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, serverAuth

[ crl_ext ]
# Extension for CRLs (man x509v3_config).
authorityKeyIdentifier=keyid:always

[ ocsp ]
# Extension for OCSP signing certificates (man ocsp).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning
EOF
```

### key CA creation

Create the root key (ca.key) and keep it absolutely secure. Anyone in possession of the root key can issue trusted certificates. Encrypt the root key with AES 256-bit encryption and a strong password.  

Use 4096 bits for all root and intermediate certificate authority keys. You’ll still be able to sign server and client certificates of a shorter length.  

```shell
cd $ROOTCADIR
openssl genrsa -aes256 -out private/ca.key 4096
chmod 400 private/ca.key
```

### Creation of the root ca cert

```shell
cd $ROOTCADIR
openssl req -config openssl.conf -key private/ca.key -new -x509 -days 3650 -sha256 -extensions v3_ca -out certs/ca.crt
```

enter the key passphrase, and other attributes. Then change permissions:

```shell
chmod 444 certs/ca.crt
```

Verify:

```shell
openssl x509 -noout -text -in certs/ca.crt
```

## intermediate CA creation

An intermediate certificate authority (CA) is an entity that can sign certificates on behalf of the root CA. The root CA signs the intermediate certificate, forming a chain of trust.
The purpose of using an intermediate CA is primarily for security. The root key can be kept offline and used as infrequently as possible. If the intermediate key is compromised, the root CA can revoke the intermediate certificate and create a new intermediate cryptographic pair.

### Preparing folders

The root CA files are kept in the rootca folder. Choose a different directory to store the intermediate CA files.  

```shell
mkdir $ROOTCADIR/intermediate
```

Create the same directory structure used for the root CA files. It’s convenient to also create a csr directory to hold certificate signing requests.
Add a crlnumber file to the intermediate CA directory tree. crlnumber is used to keep track of certificate revocation lists.  

```shell
cd $ROOTCADIR/intermediate
mkdir certs crl csr newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial
echo 1000 > crlnumber
```

### Preparing openssl.conf

```shell
cat << EOF > $ROOTCADIR/intermediate/openssl.conf
[ ca ]
# man ca
default_ca = CA_default

[ CA_default ]
# Directory and file locations.
dir               = $ROOTCADIR/intermediate
certs             = $dir/certs
crl_dir           = $dir/crl
new_certs_dir     = $dir/newcerts
database          = $dir/index.txt
serial            = $dir/serial
RANDFILE          = $dir/private/.rand

# The root key and root certificate.
private_key       = $dir/private/intermediate.key
certificate       = $dir/certs/intermediate.crt

# For certificate revocation lists.
crlnumber         = $dir/crlnumber
crl               = $dir/crl/intermediate.crl
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 3650
preserve          = no
policy            = policy_loose

[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of man ca.
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the ca man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the req tool (man req).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = FR
stateOrProvinceName_default     = IDF
localityName_default            = Paris
0.organizationName_default      = Corp
organizationalUnitName_default  = Lab
emailAddress_default            =

[ v3_ca ]
# Extensions for a typical CA (man x509v3_config).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (man x509v3_config).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:0
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
# Extensions for client certificates (man x509v3_config).
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (man x509v3_config).
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, serverAuth

[ crl_ext ]
# Extension for CRLs (man x509v3_config).
authorityKeyIdentifier=keyid:always

[ ocsp ]
# Extension for OCSP signing certificates (man ocsp).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning
EOF
```

### creating the intermediate key

Create the intermediate key (intermediate.key).  
Encrypt the intermediate key with AES 256-bit encryption and a strong password.  

```shell
cd $ROOTCADIR
openssl genrsa -aes256 -out intermediate/private/intermediate.key 4096
chmod 400 intermediate/private/intermediate.key
```

### Creating the intermediate cert

Use the intermediate key to create a certificate signing request (CSR).  
The details should generally match the root CA. The Common Name, however, must be different.

```shell
cd $ROOTCADIR
openssl req -config intermediate/openssl.conf -new -sha256 -key intermediate/private/intermediate.key -out intermediate/csr/intermediate.csr
openssl ca -config openssl.conf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in intermediate/csr/intermediate.csr -out intermediate/certs/intermediate.crt
chmod 444 intermediate/certs/intermediate.crt
```

### Verify the intermediate cert

As we did for the root certificate, check that the details of the intermediate certificate are correct.  

```shell
cd $ROOTCADIR
openssl x509 -noout -text -in intermediate/certs/intermediate.crt
openssl verify -CAfile certs/ca.crt intermediate/certs/intermediate.crt
```

### Create the chain file

When an application (eg, a web browser) tries to verify a certificate signed by the intermediate CA, it must also verify the intermediate certificate against the root certificate. To complete the chain of trust, create a CA certificate chain to present to the application.  
To create the CA certificate chain, concatenate the intermediate and root certificates together. We will use this file later to verify certificates signed by the intermediate CA.  

```shell
cd $ROOTCADIR
cat intermediate/certs/intermediate.crt certs/ca.crt > intermediate/certs/ca-chain.crt
chmod 444 intermediate/certs/ca-chain.crt
```

## creation certificates server+client

### creating folder

```shell
cd $ROOTCADIR
mkdir delivered
cd delivered
mkdir certs crl csr newcerts private
```

## creating sever cert

### creating the key

```shell
cd $ROOTCADIR
openssl genrsa -out delivered/private/server.corp.local.key 2048
chmod 400 delivered/private/server.corp.local.key
```

Use the private key to create a certificate signing request (CSR). The CSR details don’t need to match the intermediate CA.  
For server certificates, the Common Name must be a fully qualified domain name (eg, www.example.com), whereas for client certificates it can be any unique identifier (eg, an e-mail address).  
Note that the Common Name cannot be the same as either your root or intermediate certificate.

### creation of the csr

```shell
cd $ROOTCADIR
openssl req -config intermediate/openssl.cnf -key delivered/private/server.corp.local.key -new -sha256 -out delivered/csr/server.corp.local.csr
```

To create a certificate, use the intermediate CA to sign the CSR.  
If the certificate is going to be used on a server, use the server_cert extension.  
If the certificate is going to be used for user authentication, use the usr_cert extension.  
Certificates are usually given a validity of one year, though a CA will typically give a few days extra for convenience.

### creation of the crt

```shell
cd $ROOTCADIR
openssl ca -config intermediate/openssl.conf -extensions server_cert -days 1825 -notext -md sha256 -in delivered/csr/server.corp.local.csr -out delivered/certs/server.corp.local.crt
chmod 444 delivered/certs/server.corp.local.crt
openssl pkcs12 -export -out delivered/certs/server.corp.local.pfx -inkey delivered/private/server.key -in delivered/certs/server.corp.local.crt -certfile intermediate/certs/ca-chain.crt
```

The intermediate/index.txt file should contain a line referring to this new certificate.

### Verifying the cert

```shell
openssl x509 -noout -text -in delivered/certs/server.corp.local.crt
openssl verify -CAfile intermediate/certs/ca-chain.cert.crt delivered/certs/server.corp.local.crt
```

### Deploy cert files

You can now either deploy your new certificate to a server, or distribute the certificate to a client.  
When deploying to a server application (eg, Apache), you need to make the following files available:  
`ca-chain.crt` ; `server.corp.local.key` ; `server.corp.local.crt`  
If you’re signing a CSR from a third-party, you don’t have access to their private key so you only need to give them back the chain file (ca-chain.cert.pem) and the certificate (www.example.com.cert.pem).

## creating client certificate

### key creation

```shell
cd $ROOTCADIR
openssl genrsa -out delivered/private/client.corp.local.key 2048
chmod 400 delivered/private/client.corp.local.key
```

### creation of the client csr

```shell
cd $ROOTCADIR
openssl req -config intermediate/openssl.cnf -key delivered/private/client.corp.local.key -new -sha256 -out delivered/csr/client.corp.local.csr
```

### creation of the client crt

```shell
cd $ROOTCADIR
openssl ca -config intermediate/openssl.conf -extensions usr_cert -days 1825 -notext -md sha256 -in delivered/csr/client.corp.local.csr -out delivered/certs/client.corp.local.crt
chmod 444 delivered/certs/client.corp.local.crt
openssl pkcs12 -export -out delivered/certs/client.corp.local.pfx -inkey delivered/private/client.key -in delivered/certs/client.corp.local.crt -certfile intermediate/certs/ca-chain.crt
```

### verifying the cert

```shell
openssl x509 -noout -text -in delivered/certs/client.corp.local.crt
openssl verify -CAfile intermediate/certs/ca-chain.cert.crt delivered/certs/client.corp.local.crt
```

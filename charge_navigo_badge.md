# Charge NAVIGO.FR badge

```shell
sudo apt install pcsc-tools pcscd libpcsclite1
mkdir /usr/local/lib64
cd /usr/local/lib64
sudo ln -s /usr/lib/libpcsclite.so.1 libpcsclite.so
```

changing in firefox:  
type `about:config` in the url bar,  
add a key of type string `general.useragent.override` with value: `Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0`


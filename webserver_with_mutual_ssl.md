# webserver with mutual SSL auth

## install webserver

installer nginx

```sh
apt-get install -y nginx
```

créer un site sur :443

```sh
cd /etc/nginx/sites-available/
vi secure
```

contenu du fichier:

```ini
server {
    listen 443;
    ssl on;
    server_name server.corp.local;
    keepalive_timeout 70;

    access_log /var/log/nginx-secure.access.log;
    error_log /var/log/nginx-secure.error.log;

    ssl_certificate /etc/ssl/servercert/server.crt;
    ssl_certificate_key /etc/ssl/servercert/server.key;
    ssl_client_certificate /etc/ssl/servercert/ca-chain.pem;
    ssl_verify_client on;

    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    location / {
       # First attempt to serve request as file, then
       # as directory, then fall back to displaying a 404.
       try_files $uri $uri/ =404;
    }
    #location / {
    #    uwsgi_pass unix:///tmp/uwsgi.socket;
    #    include uwsgi_params;
    #}
}
```

## Create certificates

create directory

```sh
mkdir /etc/ssl/servercert
```

from the server where generating certificates

```sh
cd ~/github/rootca/intermediate/
scp certs/server.corp.local.pem root@192.168.63.253:/etc/ssl/servercert/server.crt
scp private/server.corp.local.key root@192.168.63.253:/etc/ssl/servercert/server.key
scp certs/ca-chain.pem root@192.168.63.253:/etc/ssl/servercert/ca-chain.pem
```

back to the webserver, create link to enable the site

```sh
ln -s /etc/nginx/sites-available/secure /etc/nginx/sites-enabled/secure
```

test configuration and restart

```sh
nginx -t
service nginx restart
```

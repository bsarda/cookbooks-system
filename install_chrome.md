# INSTALL CHROME

download latest release

```shell
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
```

launch it:

```shell
google-chrome
```


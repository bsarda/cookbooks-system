# OpenVPN and DNS resolution

## Introduction

It might happen that DNS resolution does not works in the openvpn connectivity, even if defined in the settings.  
As long as an "nslookup YOURSERVER IPOFDNSSERVER" works, then it's only a priority issue

## Change priority

execlute this command with the connection name to set priority of DNS

```shell
nmcli -p connection modify VPN_CONNECTION_NAME ipv4.dns-priority -1
```

## Overlap subnets

Because of installed software, IP ranges of remote site may be used by local machine' processes.  
For example, Docker and VMware Workstation could use 172.17.0.0/12 networks.

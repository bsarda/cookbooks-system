# ALT-TAB basic behavior

To have the basic behavior of alt-tab (not grouping the windows of same app), proceed as follows:

- Open `dconf-editor`
- Go to org/gnome/desktop/wm/keybindings
- Move the value '<Alt>Tab' from switch-applications to switch-windows
- Optionally move '<Shift><Alt>Tab' from switch-applications-backward to switch-windows-backward
- If you want switch-windows to work across desktops, not just in the current desktop, you can also uncheck org/gnome/shell/window-switcher/current-workspace-only
- Close dconf-editor
- If using X11, press <Alt>F2, then type r to restart Gnome.


# WIFI PRIORITY
# =============
open the configuration file of network manager, i.e.

```shell
sudo nano /etc/NetworkManager/system-connections/<wifi-ssid>
```

in the section   

```shell
[connection]
```

add the priority (default=0, the higher is the winner)   

```shell
autoconnect-priority=1000
```


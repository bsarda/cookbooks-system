# PlantUML

## Source and links

https://crashedmind.github.io/PlantUMLHitchhikersGuide/NetworkUsersMachines/NetworkUsersMachines.html
https://blog.anoff.io/2018-07-31-diagrams-with-plantuml/
https://blog.anoff.io/puml-cheatsheet.pdf

## SysML

https://en.wikipedia.org/wiki/Systems_Modeling_Language
https://sysml.org

## VSCode plugin

https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml

launch in vscode with Ctrl+P 

```code
ext install jebbs.plantuml
```

## Launch the server

```console
docker run -d -p 8080:8080 plantuml/plantuml-server:tomcat
```

Then access to `http://localhost:8080`

## First test

```puml
@startuml
!include <awslib/AWSCommon>
!include <awslib/Compute/Batch.puml>

node "rectangles" {
  rectangle "<color:red><$Batch></color>" as first
  rectangle "<$Batch>" as whatever
  rectangle "==label\n\n<color:blue><$Batch></color>\n[technology]\n\n Description" as whoever
}

first --> whatever
whatever -> whoever

database "MySql" {
  folder "This is my folder" {
    actor "<color:green>Jack</color>" as jack
    component comp1 [
    component 1
    is a
    verrrrryyyyy
    long component
    ]
  }
  cloud {
    () "Entity"
  }
}

note right of [comp1]
  A note can also
  be on several lines
end note

jack -> comp1
comp1 ...> Entity
@enduml
```

# Export Wordpress to Markdown

Used to export wordpress backup .xml to .md files

## Preparation

You must have:

- Docker installed and working
- Your exported file from Wordpress online

Export file is a .zip file, with a single xml file inside.  
Extract the file and rename it "wordpress-source.xml".

## Execution

```shell
docker build -t bsarda/wpexport .
docker volume create wpdata
wpexportmountpoint=$(docker volume inspect wpdata | grep Mountpoint | awk '{print $2}' | tr -d \" | tr -d ,)
sudo cp wordpress-source.xml $wpexportmountpoint
sudo mkdir "${wpexportmountpoint}/wordpress-export"
docker run -ti --name wpcont -v w:/data bsarda/wpexport
sudo cp -R ${wpexportmountpoint}/wordpress-export/* .
currentuser=$USER
sudo chown $currentuser:$currentuser -R *
docker rm wpcont
docker volume rm wpdata
docker rmi bsarda/wpexport
```

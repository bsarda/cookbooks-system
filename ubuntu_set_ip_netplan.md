# Set IP/DNS and hostname for Ubuntu Server

## Change IP/DNS

Ubuntu uses netplan to do so. follow the steps:

```shell
sudo vi /etc/netplan/00-installer-config.yaml
```

configure the file from:

```yaml
# This is the network config written by 'subiquity'
network: 
  ethernets: 
    ens33: 
      dhcp4: true
  version: 2
```

to something like

```yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    ens33:
      addresses: ['192.168.135.30/24']
      gateway4: 192.168.135.2
      nameservers:
        addresses: [192.168.135.9]
  version: 2
```

or

```yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    ens33:
      addresses: ['192.168.135.30/24']
      gateway4: 192.168.135.2
      nameservers:
        addresses:
          - 192.168.135.9
          - 8.8.8.8
  version: 2
```

then apply the new configuration

```shell
sudo netplan apply
```

## Change hostname

```shell
sudo hostnamectl set-hostname MYNAME
```

---
# theme: gaia
theme: olive
# default, uncover
marp: true
---

# **Share android scren**

---

## Enable USB debug mode

Follow this to enable USB debugging option

[Enable ADB Debugging](https://developer.android.com/studio/command-line/adb.html#Enabling)

---

## Docker and X

Authorize docker to push to X interface

```sh
xhost + local:docker
```

---

## Launch the docker container

Use the **scrcpy** util through Docker
[pierlon docker](https://github.com/pierlon/scrcpy-docker)

Then run the container with the _intel_ driver   
(change for yours - amd, intel, nvidia)

```sh
docker run --rm -i -t --privileged -v /dev/bus/usb:/dev/bus/usb -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY pierlo1/scrcpy:intel
```

---

## Launch the device screen

On the launched docker interactive, list the device if success

```console
/ # adb devices
List of devices attached
* daemon not running; starting now at tcp:5037
* daemon started successfully
RF8M92803AW unauthorized
```

---

On the smartphone, authorize the connection

```console
/ # adb devices
List of devices attached
RF8M92803AW device
```

Launch the screen

```console
/ # scrcpy
```

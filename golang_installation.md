# GOLANG INSTALLATION

```shell
GO_VER=$(wget -qO- https://golang.org/dl/ | grep -E 'downloadBox.*go[0-9\.]+.linux-amd64.tar.gz' | sed 's@.*downloadBox.*go/go@@' | sed 's@\.linux.*@@')
GO_DLLINK=$(wget -qO- https://golang.org/dl/ | grep -E 'downloadBox.*go[0-9\.]+.linux-amd64.tar.gz' | sed 's@.*href="@@' | sed 's@">.*@@')
wget ${GO_DLLINK}
sudo tar -C /usr/local -xvf go${GO_VER}.linux-amd64.tar.gz
rm go${GO_VER}.linux-amd64.tar.gz
```

add to the current profile

```shell
sudo nano /etc/profile
```

replace

```shell
export PATH
```

by

```shell
export PATH=$PATH:/usr/local/go/bin
```

change gopath (default /home/<usr>/go)

```shell
export GOPATH="/home/bsarda/github/go"
```

or more globaly:

```shell
echo GOPATH="/home/bsarda/github/go" >> ~/.profile
```

test if it works:

```shell
mkdir $GOPATH/src/hello -p
nano $GOPATH/src/hello/hello.go
```

code for testing:

```shell
package main
import "fmt"
func main() {
    fmt.Printf("hello, world\n")
}
```

launch the build and run the executable to validate:

```shell
cd $GOPATH/src/hello
go build
./hello`
```


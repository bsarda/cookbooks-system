# Audio issues on plug

if, when plugging the jack plug, there is no detection:

```shell
sudo alsactl restore
sudo alsa force-reload
```

if no more detection, cold reboot.  
last chance:

```shell
echo '# Keep snd-pcsp from being loaded as first soundcard' | sudo tee --append /etc/modprobe.d/alsa-base.conf > /dev/null
echo 'options snd-pcsp index=-2' | sudo tee --append /etc/modprobe.d/alsa-base.conf > /dev/null
echo 'alias snd-card-0 snd-hda-intel' | sudo tee --append /etc/modprobe.d/alsa-base.conf > /dev/null
echo 'alias sound-slot-0 snd-hda-intel' | sudo tee --append /etc/modprobe.d/alsa-base.conf > /dev/null
echo 'options snd-hda-intel model=dell-m6-amic' | sudo tee --append /etc/modprobe.d/alsa-base.conf > /dev/null
```


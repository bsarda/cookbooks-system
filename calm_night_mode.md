# CALM NIGHT MODE

install redshift and its gtk extension

```shell
sudo apt install -y redshift redshift-gtk gnome-shell-extension-redshift
```

edit the configuration to include geolocation, etc...:

```shell
nano ~/.config/redshift.conf
```

sample:

```shell
[redshift]
temp-day=6500
temp-night=4200
transition=1
gamma-day=1.0
gamma-night=0.85
location-provider=manual
adjustment-method=randr
[manual]
lat=48.979
lon=1.911
[randr]
;screen=1
```


# MOUSE BUTTONS

```shell
sudo apt-get install xautomation xbindkeys
xbindkeys --defaults ~/.xbindkeysrc >> ~/.xbindkeysrc
nano ~/.xbindkeysrc
```

add:

```shell
"xte 'keydown Alt_L' 'key Left' 'keyup Alt_L'"
b:11
"xte 'keydown Alt_L' 'key Right' 'keyup Alt_L'"
b:101
```

then autostart the program

```shell
mkdir -p ~/.config/autostart/
cat << EOF >> ~/.config/autostart/xbindkeys.desktop
[Desktop Entry]
Type=Application
Name=xbindkeys
Exec=/usr/bin/xbindkeys
EOF
```

